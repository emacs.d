;;; Simple rc to configure ido-ubiquitous.

(y-pseudo-require 'ido-ubiquitous)

(ido-ubiquitous-mode 1)
