;;; An Emacs initialization file. The heavy lifting is done in the
;;; loaded Emacs Lisp files.

;; "The default is nil, which means to use your init file".
(setq custom-file "~/.emacs.d/.custom.el")
(when (file-exists-p custom-file)
  (load custom-file))

(setq package-archives
      '(("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa-stable" . "http://stable.melpa.org/packages/")
        ("org" . "http://orgmode.org/elpa/")))

;; Setup package load paths.
(when (require 'package nil t)
  (package-initialize))

(add-to-list 'load-path "~/.emacs.d/lisp")
(load "y")
(setq load-path
      (append (y-find-subdirectories '("~/.emacs.d/lisp")) load-path))

(let ((debug-on-error nil))
  (dolist (rc '("os-rc"
                "ui-rc"
                "edit-rc"
                "smex-rc"
                "ido-ubiquitous-rc"
                "ace-window-rc"
                "paredit-rc"
                "mouse-copy-rc"
                "org-rc"
                "slime-rc"
                "redshank-rc"
                "imaxima-rc"
                "wolfram-mode-rc"
                "gambit-rc"
                "google-c-style-rc"))
    (with-demoted-errors "Error: %S" (load rc))))
